import React, { useState } from "react";
import axios from "axios";

export default function CreateTrip() {
  const [tripList, setTripList] = useState([
    {
      origin: "",
      destination: "",
      meanOfTransport: "",
    },
  ]);
  const [sortedTrip, setSortedTrip] = useState([
    {
      origin: "",
      destination: "",
      meanOfTransport: "",
    },
  ]);
  const [tripName, setTripName] = useState("My Trip");
  const [showSortedTrip, setShowSortedTrip] = useState(false);

  const appendTrip = () => {
    setTripList([
      ...tripList,
      {
        origin: "",
        destination: "",
        meanOfTransport: "",
      },
    ]);
    console.log(tripList);
  };

  const handleChange = (index) => (e) => {
    let newArr = [...tripList];
    newArr[index] = {
      ...tripList[index],
      [e.target.name]: e.target.value,
    };
    setTripList(newArr);
    console.log(tripList);
  };

  const handleDeleteTrip = (index) => (e) => {
    e.preventDefault();

    let newArr = [...tripList];
    newArr.splice(index, 1);
    setTripList(newArr);
  };

  const submitTrip = () => {
    axios
      .post("http://127.0.0.1:5000/api/v1/trips/", {
        name: tripName,
        boarding_pass: tripList,
      })
      .then((res) => {
        let data = res.data;
        setSortedTrip(data.boarding_pass);
        setShowSortedTrip(true);
      });
  };

  return (
    <div>
      <input
        type="text"
        name="tripName"
        value={tripName}
        onChange={(e) => setTripName(e.target.value)}
      />
      {tripList.map((item, index) => {
        return (
          <div>
            <form onSubmit={handleDeleteTrip(index)}>
              <label>
                Origin:
                <input
                  type="text"
                  name="origin"
                  value={item.origin}
                  onChange={handleChange(index)}
                />
              </label>
              <label>
                Destination:
                <input
                  type="text"
                  name="destination"
                  value={item.destination}
                  onChange={handleChange(index)}
                />
              </label>
              <label>
                Mean of transport:
                <input
                  type="text"
                  name="meanOfTransport"
                  value={item.meanOfTransport}
                  onChange={handleChange(index)}
                />
              </label>
              <button type="submit">Delete row</button>
            </form>
          </div>
        );
      })}
      <button onClick={appendTrip}>Add trip</button>
      <button onClick={submitTrip}>Submit</button>
      {showSortedTrip && (
        <div>
          Sorted Trip:
          {sortedTrip.map((item, index) => {
            return (
              <div>
                Nr.{index + 1} Origin:{item.origin} Destination:
                {item.destination} Mean of transport:{item.meanOfTransport}
              </div>
            );
          })}
        </div>
      )}
    </div>
  );
}
