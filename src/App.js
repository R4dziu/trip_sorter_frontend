import React from 'react';
import './App.css';
import CreateTrip from './components/CreateTrip';

function App() {
  return (
    <div className="App">
      <CreateTrip />
    </div>
  );
}

export default App;
